<?php

/***************************/ 
/* Radek HULAN             */
/* http://hulan.info/blog/ */
/***************************/ 

//require_once('fpdf.php');

/************************************/
/* global functions                 */
/************************************/
function hex2dec($color = "#000000"){
    $tbl_color = array();
    $tbl_color['R']=hexdec(substr($color, 1, 2));
    $tbl_color['G']=hexdec(substr($color, 3, 2));
    $tbl_color['B']=hexdec(substr($color, 5, 2));
    return $tbl_color;
}

function px2mm($px){
    return $px*25.4/72;
}

function txtSetCellX($pdf, $x){
	//filling with space until start on the X point
	$x-=11;$i=0;
	$string ='';
	while ($pdf->GetStringWidth($string) < $x){
		$i++;
		$string.=' ';	
	}
	return $string;
}

function txtentities($html, &$initWidth='', $width='', $x='', $pdf='', &$lines=1){
		//if ($width) echo $width.'<br />';
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans = array_flip($trans);
	
		if ($width/* && $initWidth+$pdf->GetStringWidth($html)>$width*$lines*/){
			//we have to try no to write out of the cell, so if the string is widther than the cell, write a "\n"
			$words = explode(' ', $html);
			$newStringFuture = ""; $newString=""; 
			foreach($words as $word){
				$newStringFuture.=(($newStringFuture=="")?'':' ').$word;
				
    		//echo number_format($initWidth, 2).' - '.number_format($initWidth+$pdf->GetStringWidth(utf8_decode(strtr($newStringFuture, $trans))), 2).' > '.$width*$lines.' - '.$newStringFuture.'<br />';
				if ($initWidth + $pdf->GetStringWidth(utf8_decode(strtr($newStringFuture, $trans)))>$width*$lines){
					$newString.="\n".txtSetCellX($pdf, $x).$word;	
					$lines++;
				}else{
					$newString.=(($newString=='')?'':' ').$word;
				}
			}
			$html=$newString;
		}
		
		$initWidth+=$pdf->GetStringWidth(utf8_decode(strtr($html, $trans)));
    
    return utf8_decode(strtr($html, $trans));
}


/************************************/
/* main class createPDF             */
/************************************/
class createPDF {

    function createPDF($_html,$_title,$_articleurl,$_author,$_date) {
        // main vars
        $this->html=$_html;               // html text to convert to PDF
        $this->title=$_title;             // article title
        $this->articleurl=$_articleurl;   // article URL
        $this->author=$_author;           // article author
        $this->date=$_date;               // date being published
        // other options
        $this->from='iso-8859-2';         // input encoding
        $this->to='cp1250';               // output encoding
        $this->useiconv=false;            // use iconv
        $this->bi=true;                   // support bold and italic tags
    }

    function _convert($s) {
        if ($this->useiconv) 
            return iconv($this->from,$this->to,$s); 
        else 
            return $s;
    }

    function run() {
        // change some win codes, and xhtml into html
        $str=array(
        '<ol>' => '<UL>',
        '</ol>' => '</UL>',
        '<br />' => '<br>',
        '<hr />' => '<hr>',
        '[r]' => '<red>',
        '[/r]' => '</red>',
        '[l]' => '<blue>',
        '[/l]' => '</blue>',
        '&#8220;' => '"',
        '&#8221;' => '"',
        '&#8222;' => '"',
        '&#8230;' => '...',
        '&#8217;' => '\'',
        'â€™' => '\''
        );
        foreach ($str as $_from => $_to) $this->html = str_ireplace($_from,$_to,$this->html);

        $pdf=new PDF('P','mm','A4',$this->title,$this->articleurl,false);
        $pdf->SetCreator("Script by Paco Morcillo, based on http://hulan.info/blog/");
        $pdf->SetDisplayMode('real');
        $pdf->SetTitle($this->_convert($this->title));
        $pdf->SetAuthor($this->author);
        $pdf->AddPage();

        // face
        $pdf->PutMainTitle($this->_convert($this->title));
        $pdf->PutMinorHeading('Article URL');
        $pdf->PutMinorTitle($this->articleurl,$this->articleurl);
        $pdf->PutMinorHeading('Author');
        $pdf->PutMinorTitle($this->_convert($this->author));
        $pdf->PutMinorHeading("Published: ".@date("F j, Y, g:i a",$this->date));
        $pdf->PutLine();
        $pdf->Ln(10);

        // html
        $pdf->WriteHTML($this->_convert(stripslashes($this->html)),$this->bi);

        // output
        $pdf->Output();

        // stop processing
        exit;
    }
} 

/************************************/
/* class PDF                        */
/************************************/
class PDF extends FPDF
{
    
              private $optionsheader;
              private $options;
              
	// Cabecera de página
	function Header()
	{
                if (!isset($this->optionsheader['logoheight'])) $this->optionsheader['logoheight'] = 5;
                if (isset($this->optionsheader['color']) && $this->optionsheader['color']!=''){
                    $this->setcolor($this->optionsheader['color'], 'draw');
                    $this->setcolor($this->optionsheader['color'], '');
                    
                    //top border
                    $this->Rect(5.5, 1, 203.5, 2, 'F');
                    $this->Line(5.5,1, 209, 1);
                    $this->Line(9.5,3, 207, 3);
                    
                    //right border
                    $this->Rect(207, 1.1, 2, 290, 'F');
                    $this->Line(207, 3, 207, 288);
                    $this->Line(209,1, 209, 291);
                    $this->Line(192,291, 209, 291);
                    
                    //icon border
                    $this->Rect(5.25, 1.1, 4.75, 12, 'F');
                    $this->Line(5.25, 1, 5.25, 13);
                    $this->Line(10, 3, 10, 13);
                    $this->Line(5.25,13, 10, 13);
                    
                    //numpage border
                    $this->Rect(192, 288, 15.1, 2.9, 'F');
                    $this->Line(192, 288, 207, 288);
                    $this->Line(192, 291, 192, 288);
                    
                }

                $this->setText('header', 16);
                $this->setXY(10,5);
                if (isset($this->optionsheader['icon'])) $this->showIcon($this->optionsheader['icon'], $this->GetX()-5, 7);
                if (isset($this->optionsheader['title'])) $this->MultiCell(((isset($this->optionsheader['landscape']) && $this->optionsheader['landscape'] )?290:130), 10, utf8_decode($this->optionsheader['title']));
                
                if (isset($this->optionsheader['logo']) && $this->optionsheader['logo']!=''){
                    list($logoWidth, $logoHeight, $logoType, $logoAttr) = getimagesize($this->optionsheader['logo']);
                    
                    $this->Image($this->optionsheader['logo'], 203-$this->optionsheader['logoheight']*$logoWidth/$logoHeight, 7, '',$this->optionsheader['logoheight'] );
                }
                $this->Ln(5);
                                
	}
	
	// Pie de página
	function Footer()
	{
                if (isset($this->optionsheader['showNumpages']) && $this->optionsheader['showNumpages']){
                    $this->SetText('numpages');
                    $this->SetXY(-10, -10);
                    $this->Cell(10,5,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'R');
                }
                    /*
	    // Posición: a 1,5 cm del final
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Número de página
	    $this->Cell(0,10,utf8_decode('Page ').$this->PageNo().'/{nb}',0,0,'C');
	    */
	}
	
	//variables of html parser
	var $B;
	var $I;
	var $U;
	var $HREF;
	var $fontList;
	var $issetfont;
	var $issetcolor;

            protected function addNewFont($name,  $i=false, $b=false, $bi=false){
                    $this->AddFont($name,'',$name.'.php');
                    
                    if ($i)
                        $this->AddFont($name,'I',$name.'i.php');
                    else $this->AddFont($name,'I',$name.'.php');
                    
                    if ($b)
                        $this->AddFont($name,'B',$name.'b.php');
                    else $this->AddFont($name,'B',$name.'.php');
                    
                    if ($bi)
                        $this->AddFont($name,'BI',$name.'bi.php');
                    else $this->AddFont($name,'BI',$name.'.php');
                    
                    //$this->SetFont('Roboto','',10);
                    
                    $this->fontList[] = $name;
            }
        
	function PDF($orientation='P', $unit='mm', $format='A4', $options=array(), $optionsheader=array())
	{
                    $this->FPDF($orientation,$unit,$format);
                    $this->B=0;
                    $this->I=0;
                    $this->U=1;
                    $this->HREF='';
                    $this->PRE=false;
                    $this->fontlist=array("Times","Courier");
                    $this->issetfont=false;
                    $this->issetcolor=false;
                    $this->articletitle=((isset($_title))?$_title:'');
                    $this->articleurl=((isset($_url))?$_url:'');
                    $this->debug=((isset($_debug))?$_debug:'');
                    $this->AliasNbPages();
                    
                    $arrayFonts = array(
                        /*array('name'=>'Alice_in_Wonderland_3', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'Champagne_Limousines', 'i'=>true, 'b'=>true, 'bi'=>true),
                        array('name'=>'CraftyGirls', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'daniel', 'i'=>true, 'b'=>true, 'bi'=>true),
                        array('name'=>'fashionism', 'i'=>true, 'b'=>true, 'bi'=>true),
                        array('name'=>'Fine_College', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'LittleBird', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'Oklahoma', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'Roboto', 'i'=>true, 'b'=>true, 'bi'=>true),
                        array('name'=>'Sketch_Block', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'timeburner', 'i'=>false, 'b'=>false, 'bi'=>false),
                        array('name'=>'Walkway', 'i'=>true, 'b'=>true, 'bi'=>true),
                        array('name'=>'new_cicle', 'i'=>true, 'b'=>true, 'bi'=>true),*/
                        array('name'=>'weblysleekui', 'i'=>true, 'b'=>true, 'bi'=>true)
                    );
                    
                    foreach($arrayFonts as $font){
                        $this->addNewFont($font['name'], $font['i'], $font['b'], $font['bi']);
                    }
                    
                    
                    $this->optionsheader = $optionsheader;
                    $this->options = $options;
	}
	
    function WriteHTML($html,$bi='', $tagsPDF='', $cellWidth='', $cellX='', $textclass=''){
        $pos = strripos($html, '<br>');
        if ($pos!==false){ $html = substr($html, 0,-4); }
        if (!$cellX) $cellX = $this->GetX();
        if (!$this->issetfont){$this->SetText($textclass); $this->SetStyle('U',false); $this->issetfont=true;}
    		
        //remove all unsupported tags
        $this->bi=$bi;
        if (!$tagsPDF){
	        if ($bi)
	            $html=strip_tags($html,"<a><img><p><br><font><tr><blockquote><h1><h2><h3><h4><pre><red><blue><ul><ol><li><hr><b><i><u><strong><em>"); 
	        else
	            $html=strip_tags($html,"<a><img><p><br><font><tr><blockquote><h1><h2><h3><h4><pre><red><blue><ul><ol><li><hr>"); 
	      }else{
	      	$html=strip_tags($html,'<blue><red>'); 	
	      }
        $html=str_replace("\n",' ',$html); //replace carriage returns by spaces
        // debug
        if ($this->debug) { echo $html; exit; }

        $html = str_replace('&trade;','™',$html);
        $html = str_replace('&copy;','©',$html);
        $html = str_replace('&euro;','€',$html);
        $html = str_replace( 'â€™', '\'', $html);

        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        $skip=false;
        $txtCellWidth=0; $linesCell=1;
        foreach($a as $i=>$e)
        {
            if (!$skip) {
                if($this->HREF)
                    $e=str_replace("\n","",str_replace("\r","",$e));
                if($i%2==0)
                {
                    // new line
                    if($this->PRE)
                        $e=str_replace("\r","\n",$e);
                    else
                        $e=str_replace("\r","",$e);
                    //Text
                    if($this->HREF) {
                        $this->PutLink($this->HREF,$e);
                        $skip=true;
                    } else if ($e){
                                          //$this->setXY($cellX, $this->getY());
                    		$this->Write(5,stripslashes(txtentities($e, $txtCellWidth, $cellWidth, $cellX, $this, $linesCell)));
                      }
                } else {
                    //Tag
                    if (substr(trim($e),0,1)=='/')
                        $this->CloseTag(strtoupper(substr($e,strpos($e,'/')+1)));
                        //$this->CloseTag($tag);
                    else {
                        //Extract attributes
                        $a2=explode(' ',$e);
                        $tag=strtoupper(array_shift($a2));
                        $attr=array();
                        foreach($a2 as $v) {
                            if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                                $attr[strtoupper($a3[1])]=$a3[2];
                        }
                        $this->OpenTag($tag,$attr, $cellX);
                    }
                }
            } else {
                $this->HREF='';
                $skip=false;
            }
        }
    }

    function OpenTag($tag,$attr, $cellX='')
    {
        //$this->Ln(5);
        //$this->Cell(10,10, ' *OPENTAG*'.$tag.'*OPENTAG* ',0, 1);
        //Opening tag
        switch($tag){
            case 'STRONG':
            case 'B':
                if ($this->bi)
                    $this->SetStyle('B',true);
                else
                    $this->SetStyle('U',false);
                break;
            case 'H1':
                $this->setX($cellX);
                $this->Ln(5);
                $this->SetText('header');
                break;
            case 'H2':
                $this->setX($cellX);
                $this->Ln(5);
                $this->SetText('header2');
                $this->SetStyle('U',false);
                break;
            case 'H3':
                $this->setX($cellX);
                $this->Ln(5);
                $this->SetText('header3');
                $this->SetStyle('U',false);
                break;
            case 'H4':
                $this->setX($cellX);
                $this->Ln(5);
                $this->SetText('header4');
                if ($this->bi)
                    $this->SetStyle('B',true);
                break;
            case 'PRE':
                $this->SetFont('Courier','',11);
                $this->SetFontSize(11);
                $this->SetStyle('B',false);
                $this->SetStyle('I',false);
                $this->PRE=true;
                break;
            case 'RED':
                $this->SetTextColor(255,0,0);
                break;
            case 'BLOCKQUOTE':
                $this->mySetTextColor(100,0,45);
                $this->Ln(3);
                break;
            case 'BLUE':
                $this->SetTextColor(0,0,255);
                break;
            case 'I':
            case 'EM':
                if ($this->bi)
                    $this->SetStyle('I',true);
                break;
            case 'U':
                $this->SetStyle('U',true);
                break;
            case 'A':
                $this->HREF=$attr['HREF'];
                break;
            case 'IMG':
                if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                    if(!isset($attr['WIDTH']))
                        $attr['WIDTH'] = 0;
                    if(!isset($attr['HEIGHT']))
                        $attr['HEIGHT'] = 0;
                    $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
                    $this->Ln(3);
                }
                break;
            case 'UL': 
                    $this->Ln(3);
                    $this->setX($cellX);
                   break;
            case 'OL': 
                    $this->Ln(3);
                    $this->setX($cellX);
                   break;
            case 'LI':
                $this->setX($cellX);
                //$this->SetFontSize(10);
                //$this->SetTextColor(51, 102, 153);
                $this->Write(5,utf8_decode(' » '));
                $this->mySetTextColor(-1);
                break;
            case 'TR':
                $this->Ln(7);
                $this->PutLine();
                break;
            case 'BR':
                $this->Ln(4);
                $this->setX($cellX);
                break;
            case 'P':
                $this->Ln(5);
                $this->SetText('paragraph');
                $this->setX($cellX);
                break;
            case 'HR':
                $this->setX($cellX);
                $this->PutLine();
                break;
            case 'FONT':
                if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                    $coul=hex2dec($attr['COLOR']);
                    $this->mySetTextColor($coul['R'],$coul['G'],$coul['B']);
                    $this->issetcolor=true;
                }
                if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                    $this->SetFont(strtolower($attr['FACE']));
                    $this->issetfont=true;
                }
                break;
             default: 
        }
    }

    function CloseTag($tag)
    {
        //$this->Ln(5);
        //$this->Cell(10,10, ' *CLOSETAG*'.$tag.'*CLOSETAG* ',0, 1);
        //Closing tag
        if ($tag=='H1' || $tag=='H2' || $tag=='H3' || $tag=='H4'){
            $this->Ln(6);
            $this->setText('header', 10);
            $this->SetStyle('U',false);
            $this->SetStyle('B',false);
            $this->mySetTextColor(-1);
        }
        if ($tag=='PRE'){
            $this->SetText('note');
            $this->PRE=false;
        }
        if ($tag=='RED' || $tag=='BLUE'){
        	$this->SetTextColor(0,0,0);
          $this->SetStyle('U',false);
          $this->SetStyle('B',false);
          $this->mySetTextColor(-1);
         }
        if ($tag=='BLOCKQUOTE'){
            $this->mySetTextColor(0,0,0);
            $this->Ln(3);
        }
        if($tag=='STRONG')
            $tag='B';
        if($tag=='EM')
            $tag='I';
        if((!$this->bi) && $tag=='B')
            $tag='U';
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='FONT'){
            if ($this->issetcolor==true) {
                $this->SetTextColor(0,0,0);
            }
            if ($this->issetfont) {
                $this->SetText('note');
                $this->issetfont=false;
            }
        }
        if ($tag=='LI'){
            $this->Ln(5);
        }
    }
    
    function showIcon($icon, $x, $y){
        if (!$icon) return;
        $this->Image($this->options['icons_url'].$icon.'.png',$x, $y,5, 5);        
    }
    
    function SetText($class, $size='', $style='', $color='', $backcolor='',$_font=''){
        $font = (($_font!='')?$_font:'weblysleekui'); //Walkway, Champagne_Limousines, daniel, fashionism,weblysleekui, new_cicle
        $fontHeader = (($_font!='')?$_font:'weblysleekui'); //Alice_in_Wonderland_3, CraftyGirls, Fine_College, LittleBird, Oklahoma, Sketch_Block
        $color = (($color!='')?$color:'black');
        
        switch($class){
            case 'header': 
                $this->SetFont($fontHeader,(($style!='')?(($style=!'-')?$style:''):'B'),(($size!='')?$size:14));
                $this->setcolor($color, 'font');
                break;
            case 'header2':
                $this->SetFont($fontHeader,(($style!='')?(($style=!'-')?$style:''):'B'),(($size!='')?$size:13));
                $this->setcolor($color, 'font');
                break;
            case 'header3':
                $this->SetFont($fontHeader,(($style!='')?(($style=!'-')?$style:''):'B'),(($size!='')?$size:12));
                $this->setcolor($color, 'font');
                break;
            case 'header4':
                $this->SetFont($fontHeader,(($style!='')?(($style=!'-')?$style:''):'B'),(($size!='')?$size:11));
                $this->setcolor($color, 'font');
                break;
            case 'paragraph':
                $this->SetFont($font,(($style!='')?(($style=!'-')?$style:''):''),(($size!='')?$size:10));
                $this->setcolor($color, 'font');
                break;
            case 'note':
                $this->SetFont($font, (($style!='')?(($style=!'-')?$style:''):''), (($size!='')?$size:8));
                $this->setcolor($color, 'font');
                break;
            case 'numpages':
                $this->SetFont($font, (($style!='')?(($style=!'-')?$style:''):''), (($size!='')?$size:7));
                $this->setcolor($color, 'font');
                break;
            case 'headertable':
                $this->SetFont($font, (($style!='')?(($style=!'-')?$style:''):''), (($size!='')?$size:10));
                $backcolor = (($backcolor!='')?$backcolor:'black');
                $this->setcolor('', 'font');
                break;
            default:
                $this->SetFont($font, (($style!='')?(($style=!'-')?$style:''):''), (($size!='')?$size:9));
                $this->setcolor($color, 'font');
                break;
        }
        if ($backcolor) $this->setcolor($backcolor);
    }

    function setcolor($color, $command='', $soft=false){
            switch($color){
                    case 'yellow': 	
                        switch($command){
                            case 'font': $this->SetTextColor('255', '255', '0'); break;
                            case 'draw': if ($soft)$this->SetDrawColor('250', '253', '155'); else $this->SetDrawColor('255', '255', '0'); break;
                            default: $this->SetFillColor('250', '253', '155'); break;
                        }
                        break;
                    case 'blue': 	
                        switch($command){
                            case 'font': $this->SetTextColor('50', '150', '250'); break;
                            case 'draw': if ($soft) $this->SetDrawColor('200', '234', '250'); else $this->SetDrawColor('50', '150', '250'); break;
                            default: $this->SetFillColor('200', '234', '250'); break;
                        }
                        break;
                    case 'grey': 	
                        switch($command){
                            case 'font': $this->SetTextColor('87', '87', '87'); break;
                            case 'draw': if ($soft)$this->SetDrawColor('195', '195', '195'); else $this->SetDrawColor('87', '87', '87'); break;
                            default: $this->SetFillColor('195', '195', '195'); break;
                        }
                        break;
                    case 'red': 	
                        switch($command){
                            case 'font': $this->SetTextColor('200', '50', '50'); break;
                            case 'draw': if ($soft)$this->SetDrawColor('234', '200', '200'); else $this->SetDrawColor('200', '50', '50'); break;
                            default: $this->SetFillColor('234', '200', '200'); break;
                        }
                        break;
                    case 'purple': 	
                        switch($command){
                            case 'font': $this->SetTextColor('144', '5', '169'); break;
                            case 'draw': if ($soft)$this->SetDrawColor('235', '175', '250'); else $this->SetDrawColor('144', '5', '169'); break;
                            default: $this->SetFillColor('235', '175', '250'); break;
                        }
                        break;
                    case 'green': 	
                        switch($command){
                            case 'font': $this->SetTextColor('0', '140', '30'); break;
                            case 'draw': if ($soft)$this->SetDrawColor('200', '234', '200'); else $this->SetDrawColor('0', '140', '30'); break;
                            default: $this->SetFillColor('200', '234', '200'); break;
                        }
                        break;
                    case 'white': 	
                        switch($command){
                            case 'font': $this->SetTextColor('87', '87', '87'); break; //actually is a dark grey
                            case 'draw': if ($soft)$this->SetDrawColor('250', '250', '250'); else $this->SetDrawColor('87', '87', '87'); break; //actually is a dark grey
                            default: $this->SetFillColor('250', '250', '250'); break; // actually is a soft grey
                        }
                        break;
                    case 'orange': 	
                        switch($command){
                            case 'font': $this->SetTextColor('250', '150', '50'); break; 
                            case 'draw': if ($soft)$this->SetDrawColor('250', '210', '160'); else $this->SetDrawColor('250', '150', '50'); break; 
                            default: $this->SetFillColor('250', '210', '160'); break; 
                        }
                        break;
                    case 'black': 	
                        switch($command){
                            case 'font':$this->SetTextColor('10', '10', '10'); break; 
                            case 'draw':if ($soft)$this->SetDrawColor('0', '0', '0'); else $this->SetDrawColor('10', '10', '10'); break; 
                            default:$this->SetFillColor('0', '0', '0'); break; 
                        }
                        break;
                    default: 	
                        switch($command){
                            case 'font':$this->SetTextColor('255', '255', '255'); break; //white
                            case 'draw':if ($soft)$this->SetDrawColorhea('0', '0', '0'); else $this->SetDrawColor('255', '255', '255'); break; //white
                            default:$this->SetFillColor('0', '0', '0'); break;  //black
                        }
                        break;
            }	
    }    
    
    function SetStyle($tag,$enable)
    {
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s) {
            if($this->$s>0)
                $style.=$s;
        }
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->mySetTextColor(-1);
    }

    function PutLine()
    {
        $this->Ln(2);
        $this->Line($this->GetX(),$this->GetY(),$this->GetX()+187,$this->GetY());
        $this->Ln(3);
    }

    function mySetTextColor($r,$g=0,$b=0){
        static $_r=0, $_g=0, $_b=0;

        if ($r==-1) 
            $this->SetTextColor($_r,$_g,$_b);
        else {
            $this->SetTextColor($r,$g,$b);
            $_r=$r;
            $_g=$g;
            $_b=$b;
        }
    }

    function PutMainTitle($title) {
        if (strlen($title)>55)
            $title=substr($title,0,55)."...";
        $this->SetTextColor(33,32,95);
        $this->SetFontSize(20);
        $this->SetFillColor(255,204,120);
        $this->Cell(0,20,$title,1,1,"C",1);
        $this->SetFillColor(255,255,255);
        $this->SetFontSize(12);
        $this->Ln(5);
    }

    function PutMinorHeading($title) {
        $this->SetFontSize(12);
        $this->Cell(0,5,$title,0,1,"C");
        $this->SetFontSize(12);
    }

    function PutMinorTitle($title,$url='') {
        $title=str_replace('http://','',$title);
        if (strlen($title)>70)
            if (!(strrpos($title,'/')==false))
                $title=substr($title,strrpos($title,'/')+1);
        $title=substr($title,0,70);
        $this->SetFontSize(16);
        if ($url!='') {
            $this->SetStyle('U',false);
            $this->SetTextColor(0,0,180);
            $this->Cell(0,6,$title,0,1,"C",0,$url);
            $this->SetTextColor(0,0,0);
            $this->SetStyle('U',false);
        } else
            $this->Cell(0,6,$title,0,1,"C",0);
        $this->SetFontSize(12);
        $this->Ln(4);
    }
    
}

?>