<?php
$type = 'TrueType';
$name = 'WalkwayObliqueBold';
$desc = array('Ascent'=>874,'Descent'=>-179,'CapHeight'=>874,'Flags'=>32,'FontBBox'=>'[-201 -179 1185 874]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>500);
$up = -133;
$ut = 30;
$cw = array(
	chr(0)=>500,chr(1)=>500,chr(2)=>500,chr(3)=>500,chr(4)=>500,chr(5)=>500,chr(6)=>500,chr(7)=>500,chr(8)=>500,chr(9)=>500,chr(10)=>500,chr(11)=>500,chr(12)=>500,chr(13)=>500,chr(14)=>500,chr(15)=>500,chr(16)=>500,chr(17)=>500,chr(18)=>500,chr(19)=>500,chr(20)=>500,chr(21)=>500,
	chr(22)=>500,chr(23)=>500,chr(24)=>500,chr(25)=>500,chr(26)=>500,chr(27)=>500,chr(28)=>500,chr(29)=>500,chr(30)=>500,chr(31)=>500,' '=>354,'!'=>194,'"'=>292,'#'=>531,'$'=>642,'%'=>750,'&'=>603,'\''=>175,'('=>233,')'=>233,'*'=>340,'+'=>560,
	','=>262,'-'=>350,'.'=>209,'/'=>350,'0'=>584,'1'=>413,'2'=>584,'3'=>584,'4'=>584,'5'=>584,'6'=>584,'7'=>584,'8'=>601,'9'=>584,':'=>244,';'=>251,'<'=>535,'='=>630,'>'=>535,'?'=>525,'@'=>840,'A'=>636,
	'B'=>642,'C'=>719,'D'=>680,'E'=>564,'F'=>505,'G'=>758,'H'=>680,'I'=>134,'J'=>486,'K'=>670,'L'=>505,'M'=>819,'N'=>680,'O'=>757,'P'=>621,'Q'=>757,'R'=>642,'S'=>642,'T'=>545,'U'=>664,'V'=>628,'W'=>970,
	'X'=>604,'Y'=>626,'Z'=>626,'['=>233,'\\'=>350,']'=>233,'^'=>298,'_'=>525,'`'=>193,'a'=>564,'b'=>564,'c'=>525,'d'=>564,'e'=>525,'f'=>267,'g'=>545,'h'=>525,'i'=>136,'j'=>136,'k'=>532,'l'=>136,'m'=>817,
	'n'=>525,'o'=>545,'p'=>564,'q'=>564,'r'=>318,'s'=>486,'t'=>263,'u'=>525,'v'=>471,'w'=>771,'x'=>467,'y'=>525,'z'=>473,'{'=>350,'|'=>233,'}'=>350,'~'=>548,chr(127)=>500,chr(128)=>584,chr(129)=>500,chr(130)=>292,chr(131)=>584,
	chr(132)=>351,chr(133)=>638,chr(134)=>345,chr(135)=>337,chr(136)=>259,chr(137)=>1012,chr(138)=>642,chr(139)=>294,chr(140)=>1010,chr(141)=>500,chr(142)=>500,chr(143)=>500,chr(144)=>500,chr(145)=>175,chr(146)=>175,chr(147)=>267,chr(148)=>276,chr(149)=>292,chr(150)=>495,chr(151)=>894,chr(152)=>366,chr(153)=>789,
	chr(154)=>486,chr(155)=>294,chr(156)=>1052,chr(157)=>500,chr(158)=>500,chr(159)=>663,chr(160)=>525,chr(161)=>194,chr(162)=>525,chr(163)=>500,chr(164)=>584,chr(165)=>645,chr(166)=>233,chr(167)=>584,chr(168)=>214,chr(169)=>560,chr(170)=>378,chr(171)=>391,chr(172)=>602,chr(173)=>592,chr(174)=>543,chr(175)=>313,
	chr(176)=>420,chr(177)=>630,chr(178)=>392,chr(179)=>426,chr(180)=>212,chr(181)=>525,chr(182)=>496,chr(183)=>209,chr(184)=>259,chr(185)=>287,chr(186)=>413,chr(187)=>381,chr(188)=>816,chr(189)=>803,chr(190)=>906,chr(191)=>525,chr(192)=>676,chr(193)=>676,chr(194)=>676,chr(195)=>676,chr(196)=>676,chr(197)=>676,
	chr(198)=>1077,chr(199)=>719,chr(200)=>564,chr(201)=>564,chr(202)=>564,chr(203)=>564,chr(204)=>134,chr(205)=>134,chr(206)=>134,chr(207)=>134,chr(208)=>680,chr(209)=>680,chr(210)=>757,chr(211)=>757,chr(212)=>757,chr(213)=>757,chr(214)=>757,chr(215)=>490,chr(216)=>757,chr(217)=>664,chr(218)=>664,chr(219)=>664,
	chr(220)=>664,chr(221)=>657,chr(222)=>425,chr(223)=>505,chr(224)=>552,chr(225)=>552,chr(226)=>552,chr(227)=>552,chr(228)=>552,chr(229)=>552,chr(230)=>984,chr(231)=>525,chr(232)=>525,chr(233)=>525,chr(234)=>525,chr(235)=>525,chr(236)=>136,chr(237)=>136,chr(238)=>136,chr(239)=>136,chr(240)=>564,chr(241)=>525,
	chr(242)=>545,chr(243)=>545,chr(244)=>545,chr(245)=>545,chr(246)=>545,chr(247)=>592,chr(248)=>545,chr(249)=>525,chr(250)=>525,chr(251)=>525,chr(252)=>525,chr(253)=>506,chr(254)=>564,chr(255)=>510);
$enc = 'cp1252';
$file = 'Walkwaybi.z';
$originalsize = 25764;
?>
