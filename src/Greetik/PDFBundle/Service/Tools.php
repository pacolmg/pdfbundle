<?php

namespace Greetik\PDFBundle\Service;

use PDF;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */
class Tools
{

    private $rootdir;

    public function __construct($_rootdir)
    {
        $this->rootdir = $_rootdir;

        require_once($this->rootdir . '/../web/bundles/pdf/plugins/fpdf/fpdf.php');
        require_once($this->rootdir . '/../web/bundles/pdf/plugins/fpdf/pdf.php');
    }

    /**
     *  New Empty PDF
     * @return PDF
     */
    public function newPDF()
    {
        return new PDF();
    }

    /**
     * Try to print the text on a pdf
     *
     * @param string $text the text to print
     * @return the PDF data
     * @author Pacolmg
     * */
    public function tryPrint($text)
    {
        $response = $this->createPDFFile(array(), 'Prueba');
        $response['data']['pdf']->WriteHTML($text);
        $response['data']['pdf']->Output();
    }

    /**
     * Get Colors
     *
     * @return the possible colors to select
     * @author Pacolmg
     */
    public function getColors()
    {
        return array('yellow', 'blue', 'grey', 'red', 'purple', 'green', 'white', 'orange', 'black');
    }

    /**
     * Create PDF File
     *
     * @param array $showFields If you need a table, the array with the fields to calculate the width you need to show it
     * @param string $title the title of the document
     * @param string $icon An icon to show with the title
     * @param string $bordercolor A border to show rounding the document
     * @param boolean $numpages True if we want to print the number of the page
     * @param string $logo A logo to show on the right side
     * @return PDF
     * @author Pacolmg
     */
    public function createPDFFile($showFields,
        $title = '',
        $icon = '',
        $bordercolor = '',
        $numpages = false,
        $logo = '',
        $logoheight = 5)
    {
        //look if the sizes of the fields fits on a paper
        $tableweight = 0;
        foreach ($showFields as $k => $v) {
            $tableweight += $showFields[$k];
        }

        /*
              $club = $this->get('security.context')->getToken()->getUser()->getClub();
	$logo = (($club->getAvatar2())?$this->container->getParameter('kernel.root_dir') . '/../web/files/club/'.$club->getId().'/'.$club->getAvatar2():$this->container->getParameter('kernel.root_dir') . '/../web/bundles/trainikfe/vendors/metronic_v1-5/img/logo.png');
        */

        $optionsheader = array('landscape' => (($tableweight > 200) ? true : false), 'showNumpages' => $numpages, 'icon' => $icon, 'title' => $title, 'logo' => $logo, 'logoheight' => $logoheight, 'color' => $bordercolor);

        if ($tableweight > 290) return array('errorCode' => '1', 'errorDescription' => 'Seleccione menos campos, si no no podrá ver el listado');
        if ($tableweight > 200) {
            $pdf = new PDF('L', 'mm', 'A4', array('icons_url' => $this->rootdir . '/../web/bundles/pdf/images/print_icons/'), $optionsheader);
            $startX = ceil((290 - $tableweight) / 2); //center the table on the paper
            $startX = (($startX) ? $startX : 2);
            $landscape = 1;
        } else {
            $pdf = new PDF('P', 'mm', 'A4', array('icons_url' => $this->rootdir . '/../web/bundles/pdf/images/print_icons/'), $optionsheader);
            $startX = ceil((200 - $tableweight) / 2); //center the table on the paper
            $startX = (($startX) ? $startX : 5);
        }

        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true, 15);
        $pdf->AddPage();

        if ($title != '') $pdf->SetTitle(utf8_decode($title));
        $pdf->SetAuthor('Greetik Soluciones SL');

        return array('errorCode' => '0', 'data' => array('pdf' => $pdf, 'startX' => $startX));
    }


    /**
     * Show Generic Table
     *
     * @param PDF $pdf The PDF to print the cells on it
     * @param int $startX The point X to start the table and center it
     * @param array $data the data to print
     * @param string $title The title of the table
     * @param array $showFields The width of the fields
     * @param array $arrayFields The title of the fields
     * @param array $arrayDates The fields that are dates
     * @param array $htmlFields The fields that are html
     * @param array $aligns The aligns of the fields
     * @param string $color The color of the table
     * @author Pacolmg
     */
    public function printGenericTable($pdf,
        $startX,
        $data,
        $title,
        $showFields,
        $arrayFields,
        $arrayDates = array(),
        $htmlFields = array(),
        $aligns = array(),
        $color = '',
        $arrayMoney = array())
    {
        $height = 5;
        $textclass = 'note';
        $euro_symbol = chr(128);

        if ($title != '') {
            //$pdf -> SetTextColor ('51', '102', '153');
            $pdf->SetXY($startX, $pdf->GetY());
            $pdf->MultiCell('', '5', utf8_decode($title), 0, 1);
            $pdf->Ln(5);
        }

        $pdf->setXY($startX, $pdf->GetY());
        //fontTitle2($pdf);
        $pdf->SetText('headertable');
        foreach ($showFields as $k => $v) {
            $pdf->Cell($arrayFields[$k], $height, utf8_decode($v), 0, 0, 'C', 1);
        }
        $pdf->Ln();

        $fill = false;
        //fontPar1($pdf);
        $pdf->SetText($textclass, '', '', '', $color);
        $pdf->setColor($color, 'draw', 1);

        $x = $startX;
        $y = $pdf->GetY();

        foreach ($data as $v) {
            //calculate if the field is bigger than the default height
            $heightF = $height;
            $bigFields = array();
            foreach ($v as $key => $valor) {
                $heightN = $this->calculateFieldHeight(@$arrayFields[$key], $height, $valor, $textclass, in_array($key, $htmlFields));
                if ($heightN > $heightF) {
                    $heightF = $heightN;
                }
                if ($heightN > $height) array_push($bigFields, $key);
            }
            if ($heightF + $y >= 200) {
                $pdf->AddPage();
                $y = $pdf->GetY();
            }

            // print each field, if its bigger we have to use multicell instead of cell
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetXY($x, $y);
            $lastImpresionMulti = 0;

            foreach ($showFields as $key => $title) {
                $valor = $v[$key];

                $alin = 'L';
                if (isset($aligns[$key]) && $aligns[$key]) $alin = $aligns[$key];

                if (in_array($key, $arrayDates)) {
                    if ($valor == '0000-00-00' || $valor == '1970-01-01') $texto = '-';
                    else $texto = date('d-m-Y', strtotime($valor));
                } else $texto = utf8_decode($valor) . ((in_array($key, $arrayMoney)) ? $euro_symbol : '');

                if (in_array($key, $bigFields)) {
                    $x = $pdf->GetX();
                    $y = $pdf->GetY();
                    if (in_array($key, $htmlFields)) {
                        $xPrehtml = $pdf->GetX();
                        $yPrehtml = $pdf->GetY();
                        $pdf->MultiCell($arrayFields[$key], $height, $fill, 0, $alin, $fill);
                        $xPoshtml = $pdf->GetX();
                        $yPoshtml = $pdf->GetY();
                    } else {
                        $pdf->MultiCell($arrayFields[$key], $height, $texto, $fill, $alin, $fill);
                    }

                    $pdf->SetXY($x, $pdf->GetY()); //Finish the filling action
                    $pdf->Cell($arrayFields[$key], $heightF - ($pdf->GetY() - $y), '', $fill, 0, $alin, $fill);//Finish the filling action
                    $pdf->SetXY($x + $arrayFields[$key], $y);
                    $lastImpresionMulti = 1;

                    if (in_array($key, $htmlFields)) {
                        //preg_match_all("|<[^\/](.*)>|U", $texto, $result);
                        $pdf->SetXY($xPrehtml, $yPrehtml);
                        $pdf->WriteHTML(utf8_encode($texto), 1, 0, $arrayFields[$key], $xPrehtml, $textclass); /*echo $xPrehtml.'<br />';*/
                        $pdf->SetXY($x + $arrayFields[$key], $y);
                        $pdf->SetText($textclass);
                    }


                } else {
                    if (in_array($key, $htmlFields)) {
                        $xPrehtml = $pdf->GetX();
                        $yPrehtml = $pdf->GetY();
                        $pdf->Cell($arrayFields[$key], $heightF, '', $fill, 0, $alin, $fill);
                        $xPoshtml = $pdf->GetX();
                        $yPoshtml = $pdf->GetY();
                        $pdf->SetXY($xPrehtml, $yPrehtml);
                        $pdf->WriteHTML(utf8_encode($texto), 1, 0, $arrayFields[$key], $xPrehtml, $textclass); /*echo $xPrehtml.'<br />';*/
                        $pdf->SetXY($xPoshtml, $yPoshtml);
                        $pdf->SetText($textclass);
                    } else {
                        $pdf->Cell($arrayFields[$key], $heightF, $texto, $fill, 0, $alin, $fill);
                    }
                    $x = $startX;
                    $y = $pdf->GetY() + $heightF;
                    $lastImpresionMulti = 0;
                }
            }
            if ($lastImpresionMulti) {
                $x = $startX;
                $y = $pdf->GetY() + $heightF;
            }
            $pdf->Ln();

            $fill = !$fill;
        }
    }

    /**
     * Print an array of images
     *
     * @param PDF $pdf The pdf where we print the images
     * @param array $arrayImages Array of images the images should have an url, a title and a comment
     * @param type $imageX the X point where we start to print
     * @param type $imageY The Y point where we start to print
     * @param type $imageWidth The width of the images
     * @param type $heightinRow The height of the row
     * @param type $distanceX The distance X between images
     * @param type $distanceY The distance Y between images
     * @return array With the point to print the images
     */
    public function printImages($pdf,
        $arrayImages,
        $imageX,
        $imageY,
        $imageWidth,
        $heightinRow,
        $distanceX,
        $distanceY,
        $border = false,
        $description = false)
    {
        $imageY += $heightinRow + $distanceY;
        //$imageX=$saveImageX;

        if ($imageY > 280) {
            $pdf->AddPage();
            //$imageX = $saveImageX;
            $imageY = $pdf->GetY();

            //refresh the images
            $cont = 0;
            foreach ($arrayImages as $k => $v) {
                $arrayImages[$k]['x'] = $imageX + ($distanceX + $imageWidth) * $cont;
                $arrayImages[$k]['y'] = $imageY;
                $cont++;
            }
        }
        foreach ($arrayImages as $defImg) {
            $pdf->SetXY($defImg['x'], $defImg['y']);
            $this->printImage($pdf, $defImg['image']['url'], $defImg['image']['title'], $defImg['image']['comments'], $imageWidth, $heightinRow, $border, $description);
        }

        return array($imageX, $imageY);
    }

    /**
     * Show Image
     *
     * @param PDF $pdf The pdf where we will print the image
     * @param string $imageurl The url of image
     * @param string $imagetitle The title of the image
     * @param string $imagecomments The comments of the image
     * @param int $imageWidth The image width
     * @param int $height The height of the block
     * @param PDF $auxPDF An auxiliar PDF to print the image an calculate the height
     * @return int the height of the block
     */
    public function printImage($pdf,
        $imageurl,
        $imagetitle,
        $imagecomments,
        $imageWidth,
        $border = false,
        $description = false,
        $height = '',
        $auxPDF = '')
    {
        $offsetX = 2.5;
        $offsetY = 2.5;

        if ($auxPDF) {
            $savePDF = $pdf;
            $pdf = $auxPDF;
        }

        $saveX = $pdf->getX();
        $saveY = $pdf->GetY();
        if (!$auxPDF) {
            //show the background
            $pdf->Cell($imageWidth + 5, $height, utf8_decode(''), $border, 0, 'C', 0);
            $pdf->SetXY($saveX, $saveY);
        }

        $pdf->Image($imageurl, $pdf->GetX() + $offsetX, $pdf->GetY() + $offsetY, $imageWidth);
        list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($imageurl);

        //$totalHeight = $pdf->GetY();
        $pdf->SetXY($saveX, $pdf->GetY() + $imageWidth * $imgHeight / $imgWidth + $offsetY);
        if ($description) {
            $pdf->Cell($imageWidth, 5, utf8_decode($imagetitle), 0, 1, 'C', 0);
            $pdf->setXY($saveX, $pdf->GetY());
            if ($imagecomments != '') $pdf->WriteHTML($imagecomments, 1, 0, $imageWidth, $saveX + $offsetX);
            $pdf->Ln(3);
        }

        $totalHeight = $pdf->GetY();
        if ($auxPDF) {
            $pdf = $savePDF;
            return $totalHeight;
        } else return $totalHeight;
    }


    /**
     * Calculate the height of a field
     *
     * @param int $width the width of the cell
     * @param int $height the height of the cell
     * @param string $text the text to print
     * @param string $textclass the class of the text
     * @param boolean $html It says if the field is an html field
     * @author Pacolmg
     */
    public function calculateFieldHeight($width, $height, $text, $textclass, $html = false)
    {
        $pdf = new PDF();
        $pdf->SetText($textclass);
        if (!$html) $pdf->MultiCell($width, $height, utf8_decode($text), 0, 'L', 0);
        else $pdf->WriteHTML($text, 1, 0, $width, 0);
        return $pdf->getY() + ($html ? 3 : 0);
    }


    /**
     * Print a line
     *
     * @param PDF $pdf the PDF to print the line on
     * @param string $color the color of the line
     * @author Pacolmg
     */
    public function showLine($pdf, $color = '')
    {
        if ($color) $pdf->setColor($color, 'draw');
        $pdf->Ln(6);
        $pdf->Line(10, $pdf->GetY(), 190, $pdf->GetY());
        $pdf->Ln(6);
    }
}
